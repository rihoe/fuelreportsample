package ee.ellermaa.fuelreport.dto;

import com.sun.media.sound.InvalidDataException;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class FuelReportDtoTest {

    private FuelReportDto fuelReportDto = new FuelReportDto();

    @Test
    public void setFuelName() throws InvalidDataException {
        fuelReportDto.setFuelName("E98");
        assertEquals("E98", fuelReportDto.getFuelName());
    }

    @Test(expected = InvalidDataException.class)
    public void setFuelNameThrowsException() throws InvalidDataException {
        fuelReportDto.setFuelName("");
    }

    @Test
    public void setFuelPrice() throws InvalidDataException {
        fuelReportDto.setFuelPrice("123.456");
        assertEquals((Double) 123.456, fuelReportDto.getFuelPrice());

        fuelReportDto.setFuelPrice("123.456");
        assertEquals((Double) 123.456, fuelReportDto.getFuelPrice());

        fuelReportDto.setFuelPrice("0");
        assertEquals((Double) 0., fuelReportDto.getFuelPrice());
    }

    @Test(expected = InvalidDataException.class)
    public void setFuelPriceThrowsExceptionWhenNegative() throws InvalidDataException {
        fuelReportDto.setFuelPrice("-123.456");
    }

    @Test(expected = InvalidDataException.class)
    public void setFuelPriceThrowsExceptionWithInvalidSymbol() throws InvalidDataException {
        fuelReportDto.setFuelPrice("12;3");
    }

    @Test
    public void setFuelAmount() throws InvalidDataException {
        fuelReportDto.setFuelAmount("123.456");
        assertEquals((Double) 123.456, fuelReportDto.getFuelAmount());

        fuelReportDto.setFuelAmount("123.456");
        assertEquals((Double) 123.456, fuelReportDto.getFuelAmount());

        fuelReportDto.setFuelAmount("0");
        assertEquals((Double) 0., fuelReportDto.getFuelAmount());
    }

    @Test(expected = InvalidDataException.class)
    public void setFuelAmountThrowsExceptionWhenNegative() throws InvalidDataException {
        fuelReportDto.setFuelAmount("-123.456");
    }

    @Test(expected = InvalidDataException.class)
    public void setFuelAmountThrowsExceptionWithInvalidSymbol() throws InvalidDataException {
        fuelReportDto.setFuelAmount("12;3");
    }

    @Test
    public void setRefuelingDate() throws InvalidDataException {
        fuelReportDto.setRefuelingDate("31.12.2020");
        LocalDate date = LocalDate.of(2020,12,31);
        assertEquals(date, fuelReportDto.getRefuelingDate());
    }

    @Test(expected = InvalidDataException.class)
    public void setRefuelingDateThrowsExceptionWithOutOfRangeValue() throws InvalidDataException {
        fuelReportDto.setRefuelingDate("13.31.2020");
    }

    @Test(expected = InvalidDataException.class)
    public void setRefuelingDateThrowsExceptionWithInvalidFormat() throws InvalidDataException {
        fuelReportDto.setRefuelingDate("12,31.2020");
    }
}

