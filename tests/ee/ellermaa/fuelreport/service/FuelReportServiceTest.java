package ee.ellermaa.fuelreport.service;

import ee.ellermaa.fuelreport.dto.FuelReportDto;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

import static ee.ellermaa.fuelreport.controller.FuelReportController.ALL;
import static org.junit.Assert.*;

public class FuelReportServiceTest {
    private FuelReportService service = new FuelReportService();
    private File goodFile;
    private File badFile;
    @Before
    public void setUp() throws Exception {
        String basePath = new File("").getAbsolutePath();
        goodFile = new File(basePath + "\\tests\\resource\\good.txt");
        badFile = new File(basePath + "\\tests\\resource\\bad.txt");
    }

    @Test
    public void parseDataFile() {
        List<FuelReportDto> data = service.parseDataFile(goodFile);
        assertEquals((Double) 3., data.get(0).getFuelAmount());
        assertEquals((Double) 2., data.get(0).getFuelPrice());
        assertEquals("98", data.get(0).getFuelName());
        assertEquals(LocalDate.of(2016, 1, 1), data.get(0).getRefuelingDate());
        assertFalse(service.hasErrors());
    }

    @Test
    public void parseDataFileReturnsNull() {
        File missingfile = new File("dummy.txt");
        List<FuelReportDto> data = service.parseDataFile(missingfile);
        assertNull(data);
    }

    @Test
    public void hasErrors() {
        service.parseDataFile(badFile);
        assertTrue(service.hasErrors());
    }

    @Test
    public void getErrors() {
        List<FuelReportDto> data = service.parseDataFile(badFile);
        assertEquals(3, service.getErrors().size());
    }

    @Test
    public void getFuelTypes() {
        List<FuelReportDto> data = service.parseDataFile(goodFile);
        List<String> types = service.getFuelTypes();
        assertEquals("95", types.get(0));
        assertEquals("98", types.get(1));
    }

    @Test
    public void buildChartData() {
        List<FuelReportDto> data = service.parseDataFile(goodFile);
        double[] actual = service.buildChartData(data, ALL);
        double[] expected = {10., 25., 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        assertArrayEquals(expected, actual, 0.01);
    }
}