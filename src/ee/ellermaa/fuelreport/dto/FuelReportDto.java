package ee.ellermaa.fuelreport.dto;

import com.sun.media.sound.InvalidDataException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FuelReportDto {
    private String fuelName;
    private Double fuelPrice;
    private Double fuelAmount;
    private LocalDate refuelingDate;

    private static final String PATTERN = "^[+]?[0-9]*[.|,]?[0-9]+$";
    private static final String DATEFORMAT = "dd.MM.yyyy";

    /**
     * @param fuelName Fuel type
     * @throws InvalidDataException when name is missing
     */
    public FuelReportDto setFuelName(String fuelName) throws InvalidDataException {
        fuelName = fuelName.trim();
        if (fuelName.length() == 0) {
            throw new InvalidDataException("Invalid fuel name: " + fuelName);
        }
        this.fuelName = fuelName;
        return this;
    }

    /**
     * @param fuelPrice Price of the fuel in floating point format. Comma and dot are supported
     * @throws InvalidDataException on invalid data
     */
    public FuelReportDto setFuelPrice(String fuelPrice) throws InvalidDataException {
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(fuelPrice);
        if (!matcher.matches()) {
            throw new InvalidDataException("Invalid fuel price: " + fuelPrice);
        }
        this.fuelPrice = Double.parseDouble(fuelPrice.replace(',', '.'));
        return this;
    }

    /**
     * @param fuelAmount Amount of the fuel in floating point format. Comma and dot are supported
     * @throws InvalidDataException on invalid data
     */
    public FuelReportDto setFuelAmount(String fuelAmount) throws InvalidDataException {
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(fuelAmount);
        if (!matcher.matches()) {
            throw new InvalidDataException("Invalid fuel amount: " + fuelAmount);
        }
        this.fuelAmount = Double.parseDouble(fuelAmount.replace(',', '.'));
        return this;
    }

    /**
     * @param refuelingDate Date when fueling happened. dd.MM.yyyy format is supported
     * @throws InvalidDataException on invalid data
     */
    public FuelReportDto setRefuelingDate(String refuelingDate) throws InvalidDataException {
        refuelingDate = refuelingDate.trim();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATEFORMAT);
        try {
            this.refuelingDate = LocalDate.parse(refuelingDate, formatter);
        } catch (DateTimeParseException ex) {
            throw new InvalidDataException("Ivalid date format: " + refuelingDate);
        }
        return this;
    }

    public String getFuelName() {
        return fuelName;
    }

    public Double getFuelPrice() {
        return fuelPrice;
    }

    public Double getFuelAmount() {
        return fuelAmount;
    }

    public LocalDate getRefuelingDate() {
        return refuelingDate;
    }
}
