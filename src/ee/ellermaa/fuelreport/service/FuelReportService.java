package ee.ellermaa.fuelreport.service;

import com.sun.media.sound.InvalidDataException;
import ee.ellermaa.fuelreport.controller.FuelReportController;
import ee.ellermaa.fuelreport.dto.FuelReportDto;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

public class FuelReportService {

    final private List<String> parsingErrors = new ArrayList<>();
    final private Set<String> fuelTypes = new HashSet<>();

    /**
     * Get data from provided file and populate errorslist with parsing errors
     *
     * @param file Data file
     * @return List of report items or null on error
     */
    public List<FuelReportDto> parseDataFile(File file) {
        try {
            return Files.readAllLines(file.toPath()).stream()
                    .map(this::parseRow)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            parsingErrors.add(e.getMessage());
        }
        return null;
    }

    /**
     * Group fueling data by months and filter by fuel type
     *
     * @param items    Fueling data
     * @param fuelType Fuel type
     * @return Array of monthly data
     */
    public double[] buildChartData(List<FuelReportDto> items, String fuelType) {
        double[] monthlyTotal = new double[12];

        items.forEach(item -> {
            if (item.getFuelName().equals(fuelType) || fuelType.equals(FuelReportController.ALL)) {
                monthlyTotal[item.getRefuelingDate().getMonthValue() - 1] += item.getFuelAmount() * item.getFuelPrice();
            }
        });

        return monthlyTotal;
    }

    /**
     * Split one row from datafile into data elements
     *
     * @param row One row from data file
     * @return Parsed DTO ot null on error
     */
    private FuelReportDto parseRow(String row) {
        FuelReportDto refuelingData = new FuelReportDto();

        String[] elements = validate(row).split("\\|");
        try {
            refuelingData
                    .setFuelName(elements[0])
                    .setFuelPrice(elements[1])
                    .setFuelAmount(elements[2])
                    .setRefuelingDate(elements[3]);
            fuelTypes.add(refuelingData.getFuelName());

            return refuelingData;
        } catch (InvalidDataException e) {
            parsingErrors.add(e.getMessage());
        }
        return null;
    }

    /**
     * Check if any parsing errors occurred
     *
     * @return true if any errors have been set
     */
    public boolean hasErrors() {
        return parsingErrors.size() > 0;
    }

    /**
     * Returns list of parsing errors
     *
     * @return List of errors
     */
    public List<String> getErrors() {
        return parsingErrors;
    }

    /**
     * Returns list of different fuels
     *
     * @return List of errors
     */
    public List<String> getFuelTypes() {
        List<String> sortedList = new ArrayList<>(fuelTypes);
        Collections.sort(sortedList);
        return sortedList;
    }

    /**
     * Remove invalid symbols from the input string
     *
     * @param row One row from data file
     * @return Clean row
     */
    private String validate(String row) {
        row = row.trim();
        if (row.length() > 0 && row.charAt(0) == '\uFEFF') {
            row = row.substring(1);
        }
        return row;
    }
}
