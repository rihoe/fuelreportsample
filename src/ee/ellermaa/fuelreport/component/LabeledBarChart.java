package ee.ellermaa.fuelreport.component;

import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

/**
 * Barchart that supports labels on bars
 */
public class LabeledBarChart<X, Y> extends BarChart<X, Y> {

    private Map<Node, TextFlow> nodeMap = new HashMap<>();

    /**
     * Constructor
     *
     * @param xAxis x-axis
     * @param yAxis y-axis
     */
    public LabeledBarChart(Axis<X> xAxis, Axis<Y> yAxis) {
        super(xAxis, yAxis);
        this.setBarGap(0.0);
        setLegendVisible(false);
    }

    /**
     * Add labels to series
     *
     * @param series      Added series
     * @param seriesIndex Series position
     */
    @Override
    protected void seriesAdded(Series<X, Y> series, int seriesIndex) {
        super.seriesAdded(series, seriesIndex);

        series.getData().forEach(item -> {
            Text text = new Text(format("%.3f", (Double) item.getYValue()));
            text.setStyle("-fx-font-size: 10pt;");

            TextFlow textFlow = new TextFlow(text);
            textFlow.setTextAlignment(TextAlignment.CENTER);

            nodeMap.put(item.getNode(), textFlow);
            this.getPlotChildren().add(textFlow);
        });
    }

    /**
     * Remove labels from removed series
     *
     * @param series Series to be removed
     */
    @Override
    protected void seriesRemoved(final XYChart.Series<X, Y> series) {
        for (Node bar : nodeMap.keySet()) {
            Node text = nodeMap.get(bar);
            this.getPlotChildren().remove(text);
        }
        nodeMap.clear();

        super.seriesRemoved(series);
    }

    /**
     * Position bar labels
     */
    @Override
    protected void layoutPlotChildren() {
        super.layoutPlotChildren();

        nodeMap.keySet().forEach(bar -> {
            TextFlow textFlow = nodeMap.get(bar);
            if (bar.getBoundsInParent().getHeight() > 30) {
                ((Text) textFlow.getChildren().get(0)).setFill(Color.BLACK);
                textFlow.resize(bar.getBoundsInParent().getWidth(), 200);
                textFlow.relocate(bar.getBoundsInParent().getMinX(), bar.getBoundsInParent().getMinY() + 10);
            } else {
                ((Text) textFlow.getChildren().get(0)).setFill(Color.GRAY);
                textFlow.resize(bar.getBoundsInParent().getWidth(), 200);
                textFlow.relocate(bar.getBoundsInParent().getMinX(), bar.getBoundsInParent().getMinY() - 20);
            }
        });
    }
}