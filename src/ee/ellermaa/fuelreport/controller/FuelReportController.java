package ee.ellermaa.fuelreport.controller;

import ee.ellermaa.fuelreport.component.LabeledBarChart;
import ee.ellermaa.fuelreport.dto.FuelReportDto;
import ee.ellermaa.fuelreport.service.FuelReportService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class FuelReportController {

    private LabeledBarChart<String, Number> fuelChart;
    public static final String ALL = "All";
    private List<FuelReportDto> items;
    private FuelReportService fuelReportService = new FuelReportService();

    public FuelReportController(LabeledBarChart<String, Number> fuelChart) {
        this.fuelChart = fuelChart;
    }

    /**
     * Select and load fueling data
     *
     * @param stage Main stage
     * @return boolean
     */
    public boolean loadData(Stage stage) {
        File file = getDataFile(stage);
        if (file == null) {
            return false;
        }
        items = fuelReportService.parseDataFile(file);
        if (fuelReportService.hasErrors()) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText(fuelReportService.getErrors().toString());
            errorAlert.showAndWait();
        }
        return true;
    }

    /**
     * Ask user to select import file
     *
     * @param stage Main stage
     * @return File | null
     */
    private File getDataFile(Stage stage) {
        final FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(new File("C:\\temp"));
        fileChooser.setTitle("Please select refueling data file");
        return fileChooser.showOpenDialog(stage);
    }

    /**
     * Populate combobox with fuel types. Select default value
     *
     * @param comboBox Fuel types combobox
     */
    public void addFuelTypes(ComboBox<String> comboBox) {
        List<String> fuelTypes = fuelReportService.getFuelTypes();
        ObservableList<String> options = FXCollections.observableArrayList(fuelTypes);
        options.add(0, ALL);
        comboBox.setItems(options);
        comboBox.getSelectionModel().selectFirst();
    }

    /**
     * Build barchart, based on fuel type
     *
     * @param fuelType Fuel type for data filtering
     */
    public void buildChart(String fuelType) {
        final double[] chartData = fuelReportService.buildChartData(items, fuelType);
        final double min = Arrays.stream(chartData).min().getAsDouble();
        final double max = Arrays.stream(chartData).max().getAsDouble();

        fuelChart.getData().clear();
        XYChart.Series<String, Number> series = new XYChart.Series<>();
        for (int month = 1; month < 13; month++) {
            final XYChart.Data<String, Number> data = new XYChart.Data<>(String.valueOf(month), chartData[month - 1]);
            data.nodeProperty().addListener((ov, oldNode, node) -> setBarColor(data, max, min));
            series.getData().add(data);
        }

        fuelChart.layout();
        fuelChart.getData().add(series);
    }

    /**
     * Set bar color based on value. Minimum - green, maximum - red, others - yellow
     *
     * @param data Bar data
     * @param max  Maximum value in chart
     * @param min  Minimum value in chart
     */
    private void setBarColor(XYChart.Data<String, Number> data, double max, double min) {
        Node node = data.getNode();
        if (data.getYValue().doubleValue() == min) {
            node.setStyle("-fx-bar-fill: green;");
        } else if (data.getYValue().doubleValue() == max) {
            node.setStyle("-fx-bar-fill: red;");
        } else {
            node.setStyle("-fx-bar-fill: yellow;");
        }
    }
}
