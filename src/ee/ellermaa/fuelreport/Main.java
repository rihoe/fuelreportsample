package ee.ellermaa.fuelreport;

import ee.ellermaa.fuelreport.component.LabeledBarChart;
import ee.ellermaa.fuelreport.controller.FuelReportController;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import static javafx.geometry.Pos.BASELINE_CENTER;
import static javafx.geometry.Pos.BASELINE_RIGHT;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        //BarChart
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LabeledBarChart<String, Number> barChart = new LabeledBarChart<>(xAxis, yAxis);
        xAxis.setLabel("Month");
        yAxis.setLabel("Cost");
        barChart.setTitle("Refueling Report");
        barChart.setAnimated(false);

        // FuelReportController
        final FuelReportController contr = new FuelReportController(barChart);

        // Fuel selection
        final ComboBox<String> fuelTypeCombo = new ComboBox<>();
        final Label comboLabel = new Label("Select fuel:");
        comboLabel.setAlignment(BASELINE_RIGHT);
        fuelTypeCombo.setOnAction(event -> contr.buildChart(fuelTypeCombo.getSelectionModel().getSelectedItem()));

        //Load button
        final Button loadingButton = new Button("Load data");
        loadingButton.setAlignment(BASELINE_CENTER);
        loadingButton.setOnAction(event -> {
            if (contr.loadData(stage)) {
                contr.addFuelTypes(fuelTypeCombo);
            }
        });

        // Layout
        HBox fuelSelection = new HBox(5, comboLabel, fuelTypeCombo);
        HBox toolbar = new HBox(20, loadingButton, fuelSelection);
        toolbar.setPadding(new Insets(15, 15, 15, 15));
        toolbar.setStyle("-fx-background-color: lightblue");

        VBox root = new VBox();
        VBox.setVgrow(barChart, Priority.ALWAYS);
        root.getChildren().addAll(toolbar, barChart);

        Scene scene = new Scene(root, 800, 600);
        stage.setScene(scene);
        stage.show();
    }
}
